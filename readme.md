# Petstore API - Pet Search and Update
This web application uses PHP Laravel framework to search and update pet data by using Petstore API

Steps to run the application:
1. Clone the project from the repo or download it as a zip to your workspace.
2. Go into "petstroe" directory.
3. Run Command: 
    composer update 
    &
    composer install
4. Run command:
    php artisan migrate
5. Run command: 
    php artisan serve
6. Open your web browser, application is running on: http://127.0.0.1:8000/pets or http://localhost:8000/pets
