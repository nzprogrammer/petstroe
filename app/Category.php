<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'category';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function Pet()
    {
        return $this->belongsToMany(Pet::class, 'pet_category', 'categoryID', 'petId');
    }
}
