<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tag extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'tag';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function Pet()
    {
        return $this->belongsToMany(Pet::class, 'pet_tag', 'tagID', 'petId');
    }
}
