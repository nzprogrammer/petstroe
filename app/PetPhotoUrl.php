<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PetPhotoUrl extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'pet_photo_url';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Pet()
    {
        return $this->belongsTo(Pet::class, 'petId', 'id');
    }
}
