<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Pet extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'pet';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function category()
    {
        return $this->belongsToMany(Category::class, 'pet_category', 'petId', 'categoryID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tag()
    {
        return $this->belongsToMany(Tag::class, 'pet_tag', 'petId', 'tagID');
    }

    /**
     * @Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function PetPhotoUrl()
    {
        return $this->hasMany(PetPhotoUrl::class, 'petId', 'id');
    }
}
