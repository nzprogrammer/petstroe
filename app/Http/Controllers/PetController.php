<?php

namespace App\Http\Controllers;

use App\Pet;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Validator;

class PetController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @param OSGiftCardService $OSGiftCardService
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Pet $petModel)
    {
        // Form data validation
        $validator = Validator::make($request->only(['petStatus', 'petID']), [
            'petStatus' => 'in:available,pending,sold',
            'petID' => 'nullable|numeric',
        ]);
        $searchError = '';  
        
        $updateMessage = !empty($request->input('updateMessage')) ? $request->input('updateMessage') : '';
        
        if ($validator->fails()) {
            $searchError = $validator->errors();
        }

        $petStatus = $request->input('petStatus');
        $petID = $request->input('petID');
        
        try {
            if (!empty($petID)) {
                $apiEndpoint = 'https://petstore.swagger.io/v2/pet/'.$petID;
            }
            else if (!empty($petStatus)) {
                $apiEndpoint = 'https://petstore.swagger.io/v2/pet/findByStatus?status='.$petStatus;   
            }
            else {
                // Default status
                $petStatus = 'available';
                $apiEndpoint = 'https://petstore.swagger.io/v2/pet/findByStatus?status=available';
            }


            //GuzzleHttp\Client
            $client = new Client(); 
            $apiRequest = $client->get(
                $apiEndpoint,
                array(
                    'timeout'           => 5, // Response timeout (seconds)
                    'connect_timeout'   => 5  // Connection timeout (seconds)
                )
            );
            $responseData = json_decode($apiRequest->getBody());

            // Single ID search return one object, add to array as same format as Status search result
            if (!empty($petID)) {
                $data[] = $responseData;    
            } else {
                $data = $responseData;
            }

            $petInserted = [];
           
            foreach ($data as $key => $pet){
                // Only process valid Pet ID from response
                if (is_int($pet->id) && ($pet->id > 0)) {
                    $petInDb = $petModel->where('id', $pet->id)->get()->toArray();
                    
                    if (empty($petInDb)) {
                        // Mass insert from one query, not insert individually 
                        $petInserted[] = ['id' => $pet->id, 'name' => $pet->name, 'status' => $pet->status];
                    } else {
                        // Only update when value in api different with db value
                        if (($petInDb[0]['name'] != $pet->name) || ($petInDb[0]['status'] != $pet->status)) {
                            $petToSave = Pet::find($pet->id);
                            $petToSave->name = $pet->name;
                            $petToSave->status = $pet->status;
                            $petToSave->update();
                        }
                    }
                } else {
                    unset($data[$key]);
                }
            }

            if (count($petInserted) >0) {
                Pet::insert($petInserted);
            }

            return view('pets', ['data' => $data, 'petStatus' => $petStatus, 'petID' => $petID, 'searchError' => $searchError, 'updateMessage' => $updateMessage]);
        
        } catch (GuzzleException $e) {
            $searchError = $e->getResponse()->getBody()->getContents();

            return view('pets', ['data' => [], 'petStatus' => $petStatus, 'petID' => $petID, 'searchError' => $searchError]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function petDetails($petId, Pet $petModel)
    {
        $searchError = '';
        if (!is_numeric($petId)) {
            $searchError = "Pet ID invalid.";
        }

        $petInDb = $petModel->where('id', $petId)->get()->toArray();
        $data = $petInDb[0];

        if (count($data) == 0) {
            $searchError = "Pet not found.";    
        }

        return view('pet', ['data' => $data, 'searchError' => $searchError]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pet  $petModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pet $petModel)
    {
        
        $validator = Validator::make($request->only(['petStatus', 'petID', 'petName']), [
            'petStatus' => 'in:available,pending,sold',
            'petID' => 'numeric',
        ]);
        $searchError = '';    
        if ($validator->fails()) {
            $searchError = $validator->errors();
        }

        $petStatus = $request->input('petStatus');
        $petID = $request->input('petID');
        $petName = $request->input('petName');

        $apiEndpoint = 'https://petstore.swagger.io/v2/pet/'.$petID;

         //GuzzleHttp\Client
        $client = new Client(); 
        try {
            $apiRequest = $client->post(
                $apiEndpoint,
                array(
                    'form_params' => array(
                        'name' => $petName,
                        'status' => $petStatus
                    )
                )
            );

            return redirect()->action('PetController@index', ['petID' =>  $petID, 'updateMessage' => "Update successful."]);

        } catch (GuzzleException $e) {
           
            $searchError = "Update faild. Due to: ".$e->getResponse()->getReasonPhrase();

            return redirect()->action('PetController@index', ['petID' =>  $petID, 'updateMessage' => $searchError]);
        }
    }
}