<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetstoreTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('petId')->index();
            $table->smallInteger('quantity');
            $table->dateTime('shipDate');
            $table->enum('status', ['placed', 'approved', 'delivered'])->comment('Order Status');
            $table->boolean('complete');
            $table->timestamps();
        });

        Schema::create('User', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 50)->unique();
            $table->string('firstName', 100);
            $table->string('lastName', 100);
            $table->string('email', 150);
            $table->string('password', 150);
            $table->string('phone', 50);
            $table->smallInteger('userStatus');
            $table->timestamps();
        });

        Schema::create('Category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->timestamps();
        });
       
        Schema::create('Tag', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->timestamps();
        });

        Schema::create('Pet', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->enum('status', ['available', 'pending', 'sold'])->comment('pet status in the store');
            $table->timestamps();
        });

        Schema::create('Pet_Category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('petId');
            $table->integer('categoryID');
            $table->string('name', 150);
            $table->timestamps();

            $table->index(['petId', 'categoryID']);
        });

        Schema::create('Pet_Tag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('petId');
            $table->integer('tagID');
            $table->string('name', 150);
            $table->boolean('wrapped');
            $table->timestamps();

            $table->index(['petId', 'tagID']);
        });

        Schema::create('Pet_Photo_Url', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('petId')->index();
            $table->string('name', 150);
            $table->string('url', 255);
            $table->boolean('wrapped');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Order');
        Schema::dropIfExists('User');
        Schema::dropIfExists('Category');
        Schema::dropIfExists('Tag');
        Schema::dropIfExists('Pet');
        Schema::dropIfExists('Pet_Category');
        Schema::dropIfExists('Pet_Tag');
        Schema::dropIfExists('Pet_Photo_Url');
    }
}
