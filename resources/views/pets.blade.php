<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Search</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"-->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 95vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .tick{
                content: "\2713";
                color: darkgreen;
             }
             .cross{
                content: "\2717";
                color: crimson;
             }
        </style>
    </head>
    <body>
        <div>
            Search by Pet status:
            {{ Form::open(array('action' => 'PetController@index')) }}
                Available:
                {{Form::radio('petStatus', 'available')}}<BR/>
                Pending:
                {{Form::radio('petStatus', 'pending')}}<BR/>
                Sold:
                {{Form::radio('petStatus', 'sold')}}<BR/>
                Invaild status:
                {{Form::radio('petStatus', 'invaildStatus')}}<BR/>
                OR search by Pet's ID
                {{Form::text('petID')}} (Valid ID: 34340, 803630, 967822, 12366)<BR/>
            
                {{Form::submit('Search', ['class'=>'btn btn-primary'])}}
            {{ Form::close() }}
        </div>
        
        @if (!empty($updateMessage))
            <div style="color:red">
                <P> {{$updateMessage}} </P>
            </div>
        @endif
        @if (!empty($searchError))
            <div style="color:red">
                Error: {{$searchError}}
            </div>
        @else
            <div>
                Total {{count($data)}} of 
                @if (!empty($petID)) 
                    PetID: <strong>{{$petID}}</strong> 
                @else
                    <strong>{{$petStatus}}</strong> status 
                @endif
                search result.
            </div>
            <table class="table-bordered" width="100%">
                    <thead>
                        <th>Pet ID</th>
                        <th>Category</th>
                        <th>Name</th>
                        <th>Photo</th>
                        <th>Tags</th> 
                        <th>Status</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @foreach($data as $pet)
                        <tr>
                            <td>
                                {{$pet->id}} 
                            </td>
                            <td>
                                @if (!empty($pet->category->name)) 
                                    {{$pet->category->name}} 
                                @endif  
                            </td>
                            <td>
                                @if (!empty($pet->name))
                                    {{$pet->name}}
                                @endif 
                            </td>
                            <td>
                                @if (isset($pet->photoUrls))
                                    @if (count($pet->photoUrls) >0)
                                        @foreach($pet->photoUrls as $photo)
                                            @if (!empty($photo)) 
                                                {{$photo}}<br>
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if (isset($pet->tags))
                                    @if (count($pet->tags) >0)
                                        @foreach($pet->tags as $tag)
                                            @if (!empty($tag->name)) 
                                                {{$tag->name}}<br>
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            </td>
                            <td width="10%">
                                {{$pet->status}}   
                            </td>
                            <td width="10%">
                            <a href="/pets/{{$pet->id}}/view"> Edit </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        @endif
    </body>
</html>