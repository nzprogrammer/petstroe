<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Search</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"-->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 95vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .tick{
                content: "\2713";
                color: darkgreen;
             }
             .cross{
                content: "\2717";
                color: crimson;
             }
        </style>
    </head>
    <body>
        @if (!empty($searchError))
            <div style="color:red">
                Error: {{$searchError}}
            </div>
        @else
            <div>
                Update {{$data['name']}} (ID:{{$data['id']}}):
                {{ Form::open(array('action' => 'PetController@update')) }}
                    Status:<BR>
                    Available:
                    {{Form::radio('petStatus', 'available', $data['status'] == 'available')}}<BR/>
                    Pending:
                    {{Form::radio('petStatus', 'pending', $data['status'] == 'pending')}}<BR/>
                    Sold:
                    {{Form::radio('petStatus', 'sold', $data['status'] == 'sold')}}<BR/><BR/>
                   
                    Name: {{Form::text('petName', $data['name'])}}<BR/><BR/>
                    {{ Form::hidden('petID', $data['id']) }}
                    {{Form::submit('Update', ['class'=>'btn btn-primary'])}}
                {{ Form::close() }}
            </div>
        @endif
    </body>
</html>